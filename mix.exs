defmodule Elixirc.MixProject do
  use Mix.Project

  def project do
    [
      app: :elixirc,
      version: "0.1.0",
      elixir: "~> 1.7",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      dialyzer: [
        flags: [:error_handling, :race_conditions, :no_opaque],
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {Elixirc, []},
      extra_applications: [:logger, :timex]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
      {:fe, "~> 0.1.2"},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false},
      {:timex, "~> 3.0"},
      {:distillery, "~> 2.1"}
    ]
  end
end

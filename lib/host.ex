defmodule Host do
  @moduledoc false

  def to_string({{ip_a, ip_b, ip_c, ip_d}, port}) do
    "#{ip_a}.#{ip_b}.#{ip_c}.#{ip_d}:#{port}"
  end
end

defmodule Parser do
  alias FE.Result
  alias FE.Result
  @moduledoc false

  @spec parseCommand(String.t) :: Result.t({Command.t, [String.t]}, :unknown_command)
  def parseCommand(message) do
    [head | args] = String.split(message)

    case String.downcase(head) do
      "pass" -> {:ok, {:pass, args}}
      "nick" -> {:ok, {:nick, args}}
      "user" -> {:ok, {:user, args}}
      "ping" -> {:ok, {:ping, args}}
      "pong" -> {:ok, {:pong, args}}
      "quit" -> {:ok, {:quit, args}}
      "echo" -> {:ok, {:echo, args}}
      _ -> {:error, :unknown_command}
    end
  end

  defp integer_string(input) do
    case is_binary(input) && Integer.parse(input) do
      {n, ""} -> Result.ok(n)
      _ -> Result.error("unable to parse #{inspect input} as integer")
    end
  end

  defp greater_equal_than(input, range) when input >= range, do: Result.ok(input)
  defp greater_equal_than(_, range), do: Result.error("failed condition >= #{range}")

  defp lesser_than(input, range) when input < range, do: Result.ok(input)
  defp lesser_than(_, range), do: Result.error("failed condition < #{range}")

  defp single_item_list([item]), do: Result.ok(item)
  defp single_item_list(value), do: Result.error("#{value} not a single item list")

  defp match_regex(pattern, input) do
    if String.match?(input, pattern) do
      Result.ok(input)
    else
      Result.error("failed condition match pattern on #{input}")
    end
  end

  @spec int_in_range(integer, integer, integer) :: Result.t(integer, String.t)
  def int_in_range(number, min, max) do
    integer_string(number)
    |> Result.and_then(&greater_equal_than(&1, min))
    |> Result.and_then(&lesser_than(&1, max))
  end

  @spec valid_nickname(String.t) :: Result.t(String.t, String.t)
  def valid_nickname(nickname) do
    Token.pattern(:nickname)
    |> Result.and_then(&match_regex(&1, nickname))
  end

  @spec valid_user(String.t) :: Result.t(String.t, String.t)
  def valid_user(user) do
    Token.pattern(:user)
    |> Result.and_then(&match_regex(&1, user))
  end

  @spec valid_realname(String.t) :: Result.t(String.t, String.t)
  def valid_realname(realname) do
    Token.pattern(:realname)
    |> Result.map(&Regex.run(&1, realname, capture: :all_but_first))
    |> Result.and_then(&single_item_list(&1))
    |> Result.map_error(fn _ -> "invalid realname" end)
  end
end

defmodule Logging do
  require Logger
  @moduledoc false

  def log_session(message, session) do
    Logger.debug("#{Session.to_string(session)}: #{message}")
  end
end

defmodule Outward do
  @moduledoc false

  @version Mix.Project.config[:version]
  defp version(), do: @version

  defp host(), do: Application.get_env(:elixirc, :server_host)

  @spec forward(:gen_tcp.socket, Response.t) :: :ok | {:error, any()}
  def forward(client, response), do: :gen_tcp.send(client, "#{response}\n")

  @spec notice(:gen_tcp.socket, String.t) :: :ok | {:error, any()}
  def notice(client, message) do
    response = %Response{
      cmd: :notice,
      msg: message,
      src: Application.get_env(:elixirc, :server_host)
    }
    forward(client, response)
  end

  @spec welcome(:gen_tcp.socket, %{:nick => String.t}) :: :ok | {:error, any()}
  def welcome(client, %{:nick => nick}) do
    message001 = %Response{
      cmd: :rpl_welcome,
      msg: "Welcome to elixirc Internet Relay Chat server",
      src: host(),
      dst: nick
    }
    forward(client, message001)
    message002 = %Response{
      cmd: :rpl_yourhost,
      msg: "Your host is #{host()}[#{"ip"}/#{
        Application.get_env(:elixirc, :server_port)
      }]",
      src: host(),
      dst: nick
    }
    forward(client, message002)
    message003 = %Response{
      cmd: :rpl_created,
      msg: "This server was created #{
        Application.get_env(:elixirc, :boot_time)
        |> Timex.to_datetime()
        |> Timex.format!("{WDshort} {Mshort} {D} {YYYY} at {h24}:{m}:{s} {Zabbr}")
      }",
      src: host(),
      dst: nick
    }
    forward(client, message003)
    message004 = %Response{
      cmd: :rpl_myinfo,
      msg: "#{host()} elixirc-#{version()} #{"usermodes"} #{"channelmodes"}",
      src: host(),
      dst: nick
    }
    forward(client, message004)
  end

  @spec ping(:gen_tcp.socket) :: :ok | {:error, any()}
  def ping(client) do
    forward(
      client,
      %Response{
        cmd: :ping,
        msg: host()
      }
    )
  end
end

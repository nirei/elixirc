defmodule Response do
  @moduledoc false
  defstruct cmd: nil, msg: "", src: nil, dst: nil

  @type server_command ::
          :rpl_welcome | :rpl_yourhost | :rpl_created | :rpl_myinfo
          | :notice | :privmsg | :ping
  @type t :: %Response{cmd: server_command | nil, msg: String.t | nil, src: String.t | nil, dst: String.t | nil}

  def source(response, src) do
    Map.put(response, :src, src)
  end

  def dest(response, dst) do
    Map.put(response, :dst, dst)
  end

  @spec command_term(server_command) :: String.t
  def command_term(command)

  def command_term(:rpl_welcome), do: "001"
  def command_term(:rpl_yourhost), do: "002"
  def command_term(:rpl_created), do: "003"
  def command_term(:rpl_myinfo), do: "004"
  def command_term(:notice), do: "NOTICE"
  def command_term(:privmsg), do: "PRIVMSG"
  def command_term(:ping), do: "PING"
  def command_term(_), do: raise(ArgumentError, message: "invalid argument")
end

defimpl String.Chars, for: Response do
  alias FE.Result
  @spec to_string(Response.t) :: String.t
  def to_string(response) do
    src = Map.fetch!(response, :src)
          |> not_nil()
          |> Result.map(&":#{&1} ")
          |> Result.unwrap_or("")
    dst = Map.fetch!(response, :dst)
          |> not_nil()
          |> Result.map(&"#{&1} ")
          |> Result.unwrap_or("")
    cmd = Map.fetch!(response, :cmd)
          |> not_nil()
          |> Result.unwrap!()
          |> Response.command_term()
    msg = Map.fetch!(response, :msg)
          |> not_nil()
          |> Result.map(&format_message(&1))
          |> Result.unwrap_or("")
    "#{src}#{cmd} #{dst}#{msg}"
  end

  @spec format_message(String.t) :: String.t
  defp format_message(msg), do: ":#{msg}"

  @spec not_nil(a) :: Result.t(a, String.t) when a: var
  defp not_nil(nil), do: Result.error("nil value")
  defp not_nil(value), do: Result.ok(value)
end

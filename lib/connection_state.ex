defmodule ConnectionState do
  require Logger
  use GenServer
  alias FE.Result
  @moduledoc false

  @type state :: :new | :id_user | :id_nick | :idle | :wait | :end
  @type event :: :nick | :user | :pong | :timeout | :quit

  @spec start_link({state, pid()}) :: {:ok, pid()} | {:error, any()}
  def start_link({state, client}) do
    GenServer.start(__MODULE__, {state, client})
  end

  @spec transition(pid(), event) :: :ok
  def transition(pid, event) do
    GenServer.call(pid, event)
  end

  @spec execute_transition(state, event) :: state | nil
  defp execute_transition(old_state, event) do
    state = state_function(old_state, event)
    Logger.debug("Received #{event}, transitioning from #{old_state} to #{state}")
    state
  end

  @spec state_function(state, event) :: state | nil
  defp state_function(state, event)

  # Normal transitions
  defp state_function(:new, :nick), do: :id_nick
  defp state_function(:new, :user), do: :id_user
  defp state_function(:id_user, :nick), do: :idle
  defp state_function(:id_nick, :user), do: :idle
  defp state_function(:idle, :timeout), do: :wait
  defp state_function(:wait, :pong), do: :idle
  # Defaults
  defp state_function(state, :nick), do: state
  defp state_function(state, :user), do: state
  defp state_function(state, :pong), do: state
  defp state_function(_, :timeout), do: :end
  defp state_function(_, :quit), do: :end
  # Fallback
  defp state_function(_state, _event), do: nil

  @spec transition_effect(pid(), state, state, event) :: :ok
  defp transition_effect(from, :id_nick, :idle, _action), do: GenServer.cast(from, :welcome)
  defp transition_effect(from, :id_user, :idle, _action), do: GenServer.cast(from, :welcome)
  defp transition_effect(from, _initial, :end, :timeout), do: GenServer.cast(from, :timeout)
  defp transition_effect(from, :idle, :wait, _action), do: GenServer.cast(from, :ping)
  defp transition_effect(_from, _initial, _final, _action), do: :ok

  # receives the incoming state and the current timer reference if any and returns
  # an appropriate timer reference
  @spec process_timeout(state, reference() | nil) :: {state, reference() | nil}
  defp process_timeout(:end, ref) do
    if ref != nil, do: Process.cancel_timer(ref)
    nil
  end
  defp process_timeout(state, ref) when state == :id_nick or state == :id_user do
    ref
  end
  defp process_timeout(state, ref) do
    if ref != nil, do: Process.cancel_timer(ref)
    timeout = Application.get_env(:elixirc, :timeout_conn)
              |> Map.fetch!(state)
    Process.send_after(self(), :timeout, timeout)
  end

  @impl true
  @spec init({state, pid()}) :: {:ok, {state, pid(), reference() | nil}}
  def init({state, client}) do
    Result.ok({state, client, process_timeout(state, nil)})
  end

  @impl true
  @spec handle_call(event, {pid(), any()}, {state, pid(), reference() | nil})
        :: {:reply, :ok | :error, {state, pid(), reference()}}
  def handle_call(event, _origin, {initial, client, timer_ref} = old_state) do
    case execute_transition(initial, event) do
      nil -> {:reply, :error, old_state}
      final ->
        transition_effect(client, initial, final, event)
        {:reply, :ok, {final, client, process_timeout(final, timer_ref)}}
    end
  end

  @impl true
  @spec handle_info(:timeout, {state, pid(), reference() | nil})
        :: {:noreply, {state, pid(), reference() | nil}}
  def handle_info(:timeout, {initial, client, timer_ref}) do
    Logger.debug("Timed out!")
    case execute_transition(initial, :timeout) do
      nil -> {:noreply, {initial, client, timer_ref}}
      final ->
        transition_effect(client, initial, final, :timeout)
        {:noreply, {final, client, process_timeout(final, nil)}}
    end
  end
end
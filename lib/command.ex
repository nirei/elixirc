defmodule Command do
  alias FE.Result
  require Logger
  @moduledoc false

  @type t :: :pass | :nick | :user | :echo | :quit

  @type command_error :: :bad_args
  @type outcome :: :ok | :error | :shutdown
  @type result ::
          {:ok | :shutdown, Server.state}
          | {:ok, {Server.state, String.t}}
          | {:error, {Server.state, command_error, String.t}}

  @doc """
    Runs a command
  """
  @spec run({t, [String.t]}, Server.state) :: result()
  def run(command, state)

  def run({:echo, args}, state) do
    Result.ok({state, Enum.join(args, " ")})
  end

  def run({:nick, [nickname]}, state), do: nick(state, nickname)
  def run({:nick, [_ | _]}, state), do: Result.error({state, :bad_args, "Too many arguments for nick"})
  def run({:nick, []}, state), do: Result.error({state, :bad_args, "No nickname specified"})

  def run({:user, [username, mode, _ | realname]}, state), do: user(state, username, mode, realname)
  def run({:user, _}, state), do: Result.error({state, :bad_args, "Invalid user arguments"})

  def run({:pass, [_pass]}, state), do: Result.error({state, :error, "Not implemented"})
  def run({:pass, _}, state), do: Result.error({state, :bad_Args, "Invalid pass arguments"})

  def run({:pong, _}, state), do: pong(state)

  def run({:quit, _args}, {_client, _session, connection} = state) do
    ConnectionState.transition(connection, :quit)
    {:shutdown, state}
  end

  @spec nick(Server.state, String.t)
        :: Result.t({Server.state, String.t}, {Server.state, command_error, String.t})
  defp nick({client, session, connection} = old_state, nickname) do
    ConnectionState.transition(connection, :nick)
    case Parser.valid_nickname(nickname) do
      {:ok, nick} ->
        state = {client, Map.put(session, :nick, nick), connection}
        Result.ok({state, "Nick changed to #{nick}"})
      {:error, _} ->
        Result.error({old_state, :bad_args, "Invalid nickname"})
    end
  end

  @spec user(Server.state, String.t, integer, String.t)
        :: Result.t(Server.state, {Server.state, command_error, String.t})
  defp user({client, old_session, connection} = old_state, username, mode, realname) do
    ConnectionState.transition(connection, :user)
    bits = Result.unwrap_or(Parser.int_in_range(mode, 0, 9), 0)
    with {:ok, user} <- Parser.valid_user(username),
         {:ok, real} <- Parser.valid_realname(Enum.join(realname, " ")) do
      session = old_session
                |> Map.put(:username, user)
                |> Map.put(:mode, bits)
                |> Map.put(:realname, real)
      Result.ok({client, session, connection})
    else
      {:error, message} -> Result.error({old_state, :bad_args, "Invalid arguments: #{message}"})
    end
  end

  def pong({_client, _session, connection} = state) do
    ConnectionState.transition(connection, :pong)
    Result.ok(state)
  end
end
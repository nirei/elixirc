defmodule Session do
  @moduledoc false

  def to_string(session) do
    host = "#{Host.to_string(session.host)}"
    keys = Map.keys(session) -- [:host]
    case keys do
      [] -> "[#{host}]"
      _ ->
        output = keys
                 |> Enum.map(fn (key) -> "#{key}: #{session[key]}" end)
                 |> Enum.join(", ")
        "[#{host}, {#{output}}]"
    end
  end
end

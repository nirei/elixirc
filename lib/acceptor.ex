defmodule Acceptor do
  require Logger

  def accept(port) do
    {:ok, socket} =
      :gen_tcp.listen(port, [:binary, packet: :line, active: true, reuseaddr: true])
    Logger.info("Accepting connections on port #{port}")
    loop_acceptor(socket)
  end

  defp loop_acceptor(socket) do
    {:ok, client} = :gen_tcp.accept(socket)
    {:ok, host} = :inet.peername(client)
    Logger.info("New connection from #{Host.to_string(host)}")
    #    {:ok, pid} = DynamicSupervisor.start_child(
    #      Acceptor.ConnectionSupervisor,
    #      {Server, {client, %{host: host}}}
    #    )
    # TODO: Supervise this
    {:ok, pid} = GenServer.start(Server, {client, %{host: host}})
    :ok = :gen_tcp.controlling_process(client, pid)
    loop_acceptor(socket)
  end

end
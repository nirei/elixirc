defmodule Elixirc do
  @moduledoc false
  use Application

  def start(_type, _args) do
    children = [
      {Task.Supervisor, name: Acceptor.TaskSupervisor},
      {DynamicSupervisor, name: Acceptor.ConnectionSupervisor, strategy: :one_for_one},
      {Task, fn -> Acceptor.accept(Application.get_env(:elixirc, :server_port)) end}
    ]

    opts = [strategy: :one_for_one, name: Acceptor.Supervisor]
    Supervisor.start_link(children, opts)
  end

end

defmodule Token do
  alias FE.Result
  @moduledoc "Implements all necessary tokens described on RFC 1459 parsing"

  @type t :: :user | :nickname | :letter | :digit | :special | :hexdigit | :realname

  @spec pattern(t) :: Result.t(Regex.t, any)
  def pattern(name) do
    regex = token(name)
    Regex.compile(regex)
  end

  defp concat(first, second), do: "(?:#{first})(?:#{second})"
  defp union(first, second), do: "(?:#{first}|#{second})"
  defp kleene_star(regex), do: "(?:#{regex})*"
  defp kleene_plus(regex), do: "(?:#{regex})+"
  defp quantify(regex, min), do: "(?:#{regex}){#{min},}"
  defp quantify(regex, min, max), do: "(?:#{regex}){#{min},#{max}}"

  @spec token(t) :: String.t
  defp token(term)

  defp token(:realname),
       do: ":(" <> kleene_plus("[^\\x00\\x0A\\x0D]") <> ")"
  defp token(:user),
       do: "[^\\x00\\x0A\\x0D\\x20\\x40]"
           |> kleene_plus()
  defp token(:nickname),
       do: union(token(:letter), token(:special))
           |> concat(
                (token(:letter)
                 |> union(token(:digit))
                 |> union(token(:special))
                 |> union("\-")
                 |> quantify("0", "8"))
              )

  defp token(:letter), do: "[A-Za-z]"
  defp token(:digit), do: "[0-9]"
  defp token(:special), do: "[\[\]\\`_^{|}]"
  defp token(:hexdigit),
       do: pattern(:digit)
           |> union("[A-F]")
  defp token(_), do: raise(ArgumentError, message: "invalid argument")
end

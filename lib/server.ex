defmodule Server do
  @moduledoc false
  use GenServer
  alias FE.Result

  @type state :: {:gen_tcp.socket, map(), pid()}
  @type call :: :timeout | :welcome | :ping

  def start_link(state) do
    GenServer.start(__MODULE__, state)
  end

  defp disconnect() do
    Process.exit(self(), :normal)
  end

  @spec command_result(:gen_tcp.socket, Command.result) :: state()
  defp command_result(from, {:ok, {{_client, session, _connection} = state, message}}) do
    Logging.log_session("Answer: #{message}", session)
    Outward.notice(from, message)
    state
  end
  defp command_result(_from, {:ok, state}), do: state
  defp command_result(_from, {:error, {{_client, session, _connection} = state, _error, message}}) do
    Logging.log_session("Error: #{message}", session)
    state
  end
  defp command_result(_from, {:shutdown, {_client, session, _connection}}) do
    Logging.log_session("Session finished.", session)
    disconnect()
  end

  defp execute_command(from, state, command) do
    command_result(from, Command.run(command, state))
  end

  @spec process_message(port(), state, String.t) :: state
  defp process_message(from, {_client, session, _connection} = state, message) do
    clean = "#{String.replace(message, "\n", "", global: true)}"
    Logging.log_session("Query: #{clean}", session)

    case Parser.parseCommand(message) do
      {:ok, command} -> execute_command(from, state, command)
      {:error, :unknown_command} ->
        Outward.notice(from, "Unknown command #{clean}")
        state
    end
  end

  @impl true
  @spec init({:gen_tcp.socket, map()}) :: {:ok, {map(), pid()}}
  def init({client, session}) do
    Logging.log_session("Session started", session)
    Outward.notice(client, "Awaiting user ID")
    {:ok, pid} = DynamicSupervisor.start_child(
      Acceptor.ConnectionSupervisor,
      {ConnectionState, {:new, self()}}
    )
    Result.ok({client, session, pid})
  end

  @impl true
  @spec handle_cast(call, state) :: {:noreply, state}
  def handle_cast(:welcome, {client, session, _connection_state} = state) do
    Outward.welcome(client, session)
    {:noreply, state}
  end
  def handle_cast(:timeout, {client, session, _connection_state} = state) do
    Outward.notice(client, "Session timeout reached. Come back later!")
    Logging.log_session("Session timeout reached. Closing connection.", session)
    disconnect()
    {:noreply, state}
  end
  def handle_cast(:ping, {client, _session, _connection_state} = state) do
    Outward.ping(client)
    {:noreply, state}
  end

  @impl true
  def handle_info({:tcp, client, message}, old_state) do
    state = process_message(client, old_state, message)
    {:noreply, state}
  end
  def handle_info({:tcp_closed, _socket}, {_client, session, _connection_state}) do
    Logging.log_session("Connection closed", session)
    disconnect()
  end
  def handle_info({:tcp_error, _socket, reason}, {_client, session, _connection_state}) do
    Logging.log_session("Connection error: " <> reason, session)
    Process.exit(self(), :error)
  end
end
